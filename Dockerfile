FROM php:7.3-apache
#Install git
RUN apt-get update \
    &amp;&amp; apt-get install -y git
RUN docker-php-ext-install pdo pdo_mysql mysqli
RUN a2enmod rewrite
#Install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=. --filename=composer
RUN mv composer /usr/local/bin/
ADD https://github.com/forceworkbench/forceworkbench/archive/46.0.0.tar.gz ./
RUN tar -xzvf 46.0.0.tar.gz -C /var/www/html/ && rm -rf 46.0.0.tar.gz
RUN cd /var/www/html/ forceworkbench-46.0.0 && composer install
EXPOSE 80
